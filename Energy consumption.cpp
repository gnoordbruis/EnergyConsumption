// Energy consumption.cpp : This file contains the 'main' function. Program execution begins and ends there.


// Windows imports
#include <Windows.h>

#include <curl/curl.h>
#include <fstream>
#include <iostream>
#include <string>
#include <stdio.h>
#include <streambuf>
#include <vector>
#include <chrono>

#include <time.h>
#include <typeinfo>

// Includes for tinkerforge voltage/current bricklet
#include "ip_connection.h"
#include "bricklet_voltage_current_v2.h"

using namespace std::chrono;
using namespace std;

//#define CURL_STATICLIB
#define HOST "localhost"
#define PORT 4223
#define UID "NXx"

//Global variables
//Bricklet
IPConnection ipcon;
VoltageCurrentV2 vc;

struct measurment {
	__int64 timestamp = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
	int32_t power;
};

std::vector<measurment> measurmentData;



static size_t write(void* buffer, size_t size, size_t nmemb, void* param)
{
    std::string& text = *static_cast<std::string*>(param);
    size_t totalsize = size * nmemb;
    text.append(static_cast<char*>(buffer), totalsize);
    return totalsize;
}


int retrieve_file(string* result, const char* url) {
	CURLcode res;
	// Start curl environment
	CURL* curl = curl_easy_init();
	curl_global_init(CURL_GLOBAL_DEFAULT);

	if (url == "") {
		url = "http://txt2html.sourceforge.net/sample.txt";
	}


	if (curl) {
		curl_easy_setopt(curl, CURLOPT_URL, url);
		// Set callback function to write 
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write);
		// Set a pointer to result string in memory
		// Faster than writing to a file, unsure about the true time impact.
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, result);

		// Switch on full protocol/debug output
		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

		// Perform retrieval
		res = curl_easy_perform(curl);

		// Cleanup
		curl_easy_cleanup(curl);

		if (res != CURLE_OK) {
			// Curl failed
			std::cerr << "curl error: " << res << '\n';
			return 1;
		}
	}
	else {
		return 1;
	}
    // Release everything from curl_global_init
    curl_global_cleanup();
	return 0;
}



// Callback function for current callback
void callBackPower(int32_t power, void* user_data) {
    (void)user_data; // Avoid unused parameter warning

	measurment dataPoint;
	dataPoint.power = power;
	measurmentData.push_back(dataPoint);
	//printf("Current: %f W\n", power / 1000.0);
}

void output(string *result, string filename, string url) {
	// Output to file with filename
	std::ofstream outfile(filename);

	double avgPower = 0;
	double energy;
	//Duration of query in ms 
	__int64 duration = measurmentData.back().timestamp - measurmentData[0].timestamp;

	outfile << "Filename: " << filename << "\n";
	outfile << "Used URL: " << url << "\n\n";
	for (auto x : measurmentData) {
		avgPower += x.power;
		outfile << x.timestamp << "\t" << x.power << "\n";
	}
	if (avgPower != 0) {
		avgPower = avgPower / measurmentData.size();
	}
	else {
		cout << "\nERROR: avgPower = 0\n";
	}
	energy = avgPower * duration;

	outfile << "\nDuration: \t" << duration <<"ms\n";
	outfile << "avgPower: \t" << avgPower << "mW\n";
	outfile << "energy: \t" << energy << "mJ\n";

	outfile << "\n\n" << "Result:\n" << *result;
	outfile.close();


}

int main(void) {
	string url;
	string filename;
	string result;
	
	
	cout << "Input output filename\n";
	cin >> filename;
	cout << "Input URL\n";
	cin >> url;
	cout << url << "\t" << filename;
	
	char urlArr[1024];
	strcpy_s(urlArr, url.c_str());
	
	// Create IP connection
	ipcon_create(&ipcon);
	// Create device object
	voltage_current_v2_create(&vc, UID, &ipcon);
	// Connect to brick deamon
	if (ipcon_connect(&ipcon, HOST, PORT) < 0) {
		cout << "Could not connect\n";
		return 1;
	}
	// Register current callback to function callBackPower
	voltage_current_v2_register_callback(&vc, VOLTAGE_CURRENT_V2_CALLBACK_POWER, (void (*)(void))callBackPower, NULL);
	// Set period for current callback to 1s (1000ms) without a threshold
	voltage_current_v2_set_power_callback_configuration(&vc, 10, false, 'x', 0, 0);

    printf("Wait a bit\n");
    getchar();

	
	int retrieval = retrieve_file(&result, urlArr);

	voltage_current_v2_destroy(&vc);
	ipcon_destroy(&ipcon); // Calls ipcon_disconnect internally

	output(&result, filename, url);

    //std::ifstream check("w3ISO8859.txt");


	//cout << result.compare(str);

    return 0;
}
